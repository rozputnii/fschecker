using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace FileSystemChecker.Console
{
    public static class UrlExt
    {
        public static string ToQueryString(this NameValueCollection nvc)
        {
            return nvc == null ? null : string.Join("&", nvc.GetUrlList());
        }

        public static IEnumerable<string> GetUrlList(this NameValueCollection nvc)
        {
            foreach (var k in nvc.AllKeys)
            {
                var values = nvc.GetValues(k);
                if (values?.Length > 0)
                {
                    var list = $"{k}=";
                    for (var i = 0; i < values.Length; i++)
                    {
                        if (i != values.Length - 1)
                            list += $"{values[i] ?? string.Empty},";
                        else
                            list += $"{values[i] ?? string.Empty}";
                    }
                    yield return list;
                    continue;
                }
                yield return k;
            }
        }
    }
}