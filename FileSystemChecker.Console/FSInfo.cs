﻿using System;

namespace FileSystemChecker.Console
{
    public class FSInfo
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string EventType { get; set; }
        public string ObjectType { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
    }
}