﻿using System.Net;
using static System.Console;

namespace FileSystemChecker.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            WriteLine("Enter host: (example: http://localhost:57536)");
            var serverPath = ReadLine();
            WriteLine(@"Enter directory: (example: c:\\)");
            var directoryPath = ReadLine();
            var service = new SystemInfoService(serverPath, directoryPath, true);
            service.StartMonitoring();
            ReadLine();
        }
    }
}
