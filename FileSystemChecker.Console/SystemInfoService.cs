using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;

namespace FileSystemChecker.Console
{
    public class SystemInfoService : IDisposable
    {

        private readonly string _path;
        public SystemInfoService(string serverPath, string directoryPath, bool includeSubdirectories)
        {
            _path = serverPath;
            _watcher = new FileSystemWatcher();
            if (!string.IsNullOrWhiteSpace(directoryPath))
                _watcher.Path = directoryPath;
            _watcher.IncludeSubdirectories = includeSubdirectories;
            _watcher.Changed += WatcherOnChanged;
            _watcher.Created += WatcherOnChanged;
            _watcher.Deleted += WatcherOnChanged;
            _watcher.Renamed += WatcherOnChanged;
            _watcher.Error += WatcherOnError;
        }

        private readonly FileSystemWatcher _watcher;

        public void SetPath(string path)
        {
            try
            {
                var temp = _watcher.EnableRaisingEvents;
                _watcher.EnableRaisingEvents = false;
                _watcher.Path = path;
                if (temp)
                    _watcher.EnableRaisingEvents = true;
            }
            catch (Exception exception)
            {
                throw;
            }
        }
        public bool StartMonitoring()
        {
            try
            {
                _watcher.EnableRaisingEvents = true;
                return true;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        private void WatcherOnError(object sender, ErrorEventArgs errorEventArgs)
        {
            System.Console.WriteLine($"{DateTime.Now.ToString("dd.MM.yy HH:mm:ss")} Error: {errorEventArgs.GetException().Message}");

        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs args)
        {
            
            System.Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} {args.ChangeType}: {args.FullPath}");

            var isDirectory = System.IO.Path.GetExtension(args.FullPath) == "";
            SendToServer(new FSInfo
            {
                Time = DateTime.Now,
                EventType = args.ChangeType.ToString(),
                Path = args.FullPath,
                ObjectType = isDirectory? "Directory":"File",
            });
        }

        public async void SendToServer(FSInfo info)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var nvc = new NameValueCollection
                    {
                        {"path", info.Path}
                        ,
                        {"time", info.Time.ToString("G")}
                        ,
                        {"eventtype", info.EventType}
                        ,
                        {"objecttype", info.ObjectType}
                        ,
                        {"description", info.Description}
                    };
                    var request = WebRequest.Create($@"{_path}/api/info/{0}?{nvc.ToQueryString()}") as HttpWebRequest;
                    if (request != null)
                        using (var responce= await request.GetResponseAsync())
                        using (var stream = responce.GetResponseStream())
                        using (var sr=new StreamReader(stream??Stream.Null))
                        {
                            var result = await sr.ReadToEndAsync();
                            
                            System.Console.WriteLine(result == "true"?"Success":"Fail");
                        }
                }
            }
            catch(Exception exception)
            {
                
            }
        }

        //public HttpWebRequest BuildRequest(string shortPath, Port port, NameValueCollection urlParams)
        //{
        //    var url = $@"https://www.online.telefum.com:{port:D}/telefum_online/{shortPath}";

        //    urlParams.Add("apikey", ApiKey);

        //    url += $@"?{urlParams.ToQueryString()}";

        //    var request = WebRequest.Create(url) as HttpWebRequest;
        //    if (request == null) return null;
        //    if (port == Port.Cookie)
        //        request.Headers["Cookie"] = Cookies;
        //    request.Timeout = 20000;

        //    return request;
        //}
        public void Dispose()
        {
            _watcher?.Dispose();
        }
    }
}