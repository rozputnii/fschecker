﻿using System.Data.Entity;

namespace WebApplication2.Models
{
    public class FsInfoContext: DbContext
    {
        public System.Data.Entity.DbSet<FsInfo> FsInfoes { get; set; }
    }
}