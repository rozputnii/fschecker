﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class FsInfoesController : Controller
    {
        

        // GET: FsInfoes
        public async Task<ActionResult> Index()
        {
            return View(await Repository.Instance.GetInfo());
        }

        //// GET: FsInfoes/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FsInfo fsInfo = await db.FsInfoes.FindAsync(id);
        //    if (fsInfo == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fsInfo);
        //}

        //// GET: FsInfoes/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: FsInfoes/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Time,EventType,ObjectType,Path,Description")] FsInfo fsInfo)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.FsInfoes.Add(fsInfo);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(fsInfo);
        //}

        //// GET: FsInfoes/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FsInfo fsInfo = await db.FsInfoes.FindAsync(id);
        //    if (fsInfo == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fsInfo);
        //}

        //// POST: FsInfoes/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,Time,EventType,ObjectType,Path,Description")] FsInfo fsInfo)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(fsInfo).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(fsInfo);
        //}

        //// GET: FsInfoes/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FsInfo fsInfo = await db.FsInfoes.FindAsync(id);
        //    if (fsInfo == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fsInfo);
        //}

        //// POST: FsInfoes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    FsInfo fsInfo = await db.FsInfoes.FindAsync(id);
        //    db.FsInfoes.Remove(fsInfo);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

     
    }
}
