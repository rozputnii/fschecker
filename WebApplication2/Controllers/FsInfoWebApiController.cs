﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/info")]
    public class FsInfoWebApiController : ApiController
    {
        // GET: api/FSInfoes
        [Route("")]
        public async Task<List<FsInfo>> GetFsInfoes()
        {
            return await Repository.Instance.GetInfo();
        }

        [HttpGet]
        [ResponseType(typeof(bool))]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> AddFsInfo(int id,[FromUri] FsInfo info,[FromUri]string time)
        {
            try
            {
              info.Id = default(int);
                info.Time = Convert.ToDateTime(time);
                return Ok(await Repository.Instance.Update(context => context.FsInfoes.Add(info)));
            }
            catch (Exception exception)
            {
                return Ok(false);
            }
        }
    }
}