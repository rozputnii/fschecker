using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class Repository
    {
        private FsInfoContext _db;
        private Repository()
        {
            _db = new FsInfoContext();
        }

        public FsInfoContext Context => _db;
        private static readonly object SyncObj = new object();
        private static Repository _instance;
        public static Repository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncObj)
                    {
                        var inst = new Repository();
                        Interlocked.Exchange(ref _instance, inst);
                        return _instance;
                    }
                }
                return _instance;
            }
        }

        public  Task<List<FsInfo>> GetInfo()
        {
            try
            {
                lock (SyncObj)
                {
                    return _db?.FsInfoes?.ToListAsync();
                }
            }
            catch
            {
                return null;
            }
        }

        private int _countChanges = 0;
        private void CheckForUpdate()
        {
            _countChanges++;
            if (_countChanges >= 100)
            {
                _db?.Dispose();
                _db = new FsInfoContext();
            }
        }
        public Task<bool> Update(Action<FsInfoContext> action)
        {
            return Task.Run(() =>
            {
                try
                {
                    lock (SyncObj)
                    {
                        action(_db);
                        _db?.SaveChanges();
                        CheckForUpdate();
                        return true;
                    }
                }
                catch (Exception exception)
                {
                    lock (SyncObj)
                    {
                        _db?.Dispose();
                        _db = new FsInfoContext();
                        return false;
                    }
                }
            });

        }
    }
}